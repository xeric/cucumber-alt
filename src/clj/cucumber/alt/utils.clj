(ns cucumber.alt.utils
  (:require [clojure.java.io :as io]
            [clojure.tools.namespace :as tools-ns]
            [clojure.tools.namespace.find :as ns-find]
            [clojure.tools.namespace.file :as ns-file]
            [clojure.java.classpath :as cp]
            [cucumber.alt.backend])
  (:import [cucumber.runtime CucumberException ClojureFriendlyDefaultSummaryPrinter]
           [java.util Properties]
           [java.io PrintStream ByteArrayOutputStream]
           [cucumber.alt.backend Backend]
           [cucumber.api SummaryPrinter]
           [cucumber.runtime.io FileResourceLoader MultiLoader
            ResourceLoaderClassFinder]
           [cucumber.runtime RuntimeOptions Env]))

(def CUCUMBER-OPTIONS "cucumber.options")

(defn- ^Env env [project]
  (let [^Properties props (System/getProperties)
        cucumber-options (or (:cucumber-options project)
                             (.getProperty props CUCUMBER-OPTIONS ""))]
    (when cucumber-options
      (.setProperty props CUCUMBER-OPTIONS cucumber-options))                  
    (Env. "cucumber" props)))

(defn- runtime-options
  [env args repl?]
  (proxy [RuntimeOptions]
      [env (if (some? args) args [])]
    (summaryPrinter [^ClassLoader cl]
      (if repl?
        (ClojureFriendlyDefaultSummaryPrinter. *out*)
        (proxy-super summaryPrinter cl)))))

(defn runtime
  "Passes command line options to Cucumber runtime. You can specify
   the runtime options using cucumber-options in project file"
  [project & args]
  (let [^Env env (env project)
        classloader (.getContextClassLoader (Thread/currentThread))
        resource-loader (MultiLoader. classloader)
        ^RuntimeOptions ro (runtime-options env args (empty? project))
        ^Runtime runtime (cucumber.runtime.Runtime. resource-loader classloader
                                                    [(Backend. resource-loader)] ro)]
    (.run runtime)
    {:errors (.getErrors runtime) :exit-status (.exitStatus runtime)
     :errors? (not (.isEmpty (.getErrors runtime))) :runtime runtime}))

(def in-runtime? (atom false))

(defn- map->cmd-args
  "Converts map to cucumber command line arguments."
  [m]
  (reduce-kv (fn [a k v]
               (let [opt (str "--" (name k))
                     arg (if (keyword? v) (name v) v)]
                 (cond
                   (= k v)    (conj a opt)
                   (coll? v)  (->> (map name arg)
                                   (interpose opt)
                                   (concat (conj a opt)))
                   :else      (conj a opt arg))))
             [] m))

(defn- feature-path
  [feature]
  (if-let [r (io/resource (str feature ".feature"))]
    (.getPath r)
    (throw (CucumberException.
            (format "Feature not found: %s.feature" feature)))))

(defn hook-file?
  [ns]
  (and (some? ns) (.endsWith (name ns) ".hooks")))

(defn find-namespace-files
  [namespace]
  (letfn [(match? [file]
            (let [ns (second (ns-file/read-file-ns-decl file))]
              (or (hook-file? ns) (= ns namespace))))]
    (->> (cp/classpath-directories)
         (mapcat #(tools-ns/find-clojure-sources-in-dir %1))
         (filter match?)
         (map #(.getPath %1)))))

(defn ns->glues
  [ns]
  (interleave (repeat "--glue") (find-namespace-files ns)))

(defn test-feature*
  "Test a feature with a given glue and cucumber configuration arguments.
   The arguments are mapped to the cucumber-jvm commandline arguments.
   A key value :plugin \"pretty\" is expanded as \"--plugin pretty\".
   To use an option with no arguments set the key and value to the same
   value e.g. :dry-run :dry-run is expanded to --dry-run"
  [feature ns m]
  (when-not @in-runtime?
    (reset! in-runtime? true)
    (try
      (let [feature (feature-path feature)
            glues (ns->glues ns)
            args (map->cmd-args m)]
        (apply runtime {} (conj (vec (concat args glues)) "--tags" "~@ignore" feature)))
      (finally
        (reset! in-runtime? false)))))

(defmacro test-feature
  [feature & {:as args}]
  `(test-feature* ~feature (.getName *ns*) (or ~args {})))
