(ns cucumber.alt.backend
  (:require [clojure [string :as str]]
            [clojure.test :as t]
            [clojure.java.io :as io]
            [cucumber.runtime.clj :as cucumber-clj])
  (:import [cucumber.runtime CucumberException
            JdkPatternArgumentMatcher
            StepDefinition TransformerFactory TransformerFactory$ITransformer
            HookDefinition ParameterInfo]
           [cucumber.runtime.snippets Snippet SnippetGenerator]
           [gherkin TagExpression]
           [clojure.lang RT])
  (:gen-class :name cucumber.alt.backend.Backend
              :implements [cucumber.runtime.Backend]
              :constructors
              {[cucumber.runtime.io.ResourceLoader] []}
              :init init
              :state state))

(def steps    (atom []))
(def hooks    (atom []))
(def context  (atom {}))
(def bindings (atom {}))
(def redefs   (atom {}))

(defmacro with-context [syms & body]
  `(let [{:keys ~syms} @context]
     ~@body))  

(def snippet-generator (SnippetGenerator. (cucumber-clj/clojure-snippet)))

(defn- location-str [{:keys [file line]}]
  (str file ":" line))

(defn transform-proxy
  [transform-fn]
  (when (some? transform-fn)
    (TransformerFactory/create
     (reify TransformerFactory$ITransformer
       (transform [_ ^String s]
         (transform-fn s))))))

(defn add-step-definition
 [{:keys [pattern fun location types]}]
  (.addStepDefinition
   @cucumber-clj/glue
   (reify
     StepDefinition
     (matchedArguments [_ step]
       (.argumentsFrom (JdkPatternArgumentMatcher. pattern)
                       (.getName step)))
     (getLocation [_ detail]
       (location-str location))
     (getParameterCount [_] nil)
     (getParameterType [_ n argumentType]
       (let [{:keys [tag format delimiter transposed transformer]
              :or {delimiter ParameterInfo/DEFAULT_DELIMITER
                   transposed false
                   tag java.lang.String}}
             (nth types n)]
         (if tag
           (ParameterInfo. tag format delimiter transposed
                           (transform-proxy transformer)))))
     (execute [_ locale args]
       (apply fun args))
     (isDefinedAt [_ stack-trace-element]
       (and (= (.getLineNumber stack-trace-element)
               (:line location))
            (= (.getFileName stack-trace-element)
               (:file location))))
     (getPattern [_]
       (str pattern)))))

(defn load-script [path]
  (try
    (clojure.main/load-script path)
    (catch Throwable t
      (throw (CucumberException. t)))))

(defn -init [resource-loader]
  [[] (atom {:resource-loader resource-loader})])

(defn get-resources
  [resource-loader path]
  (let [path (if-let [rs (io/resource path)] (.getPath rs) path)]
    (.resources resource-loader path ".clj")))

(defn- reset-state! []
  (reset! bindings {})
  (reset! redefs {})
  (reset! steps [])
  (reset! hooks [])
  (reset! context {}))

(defn -loadGlue [cljb a-glue glue-paths]
  (doseq [path     glue-paths
          resource (get-resources (:resource-loader @(.state cljb)) path)]
    (reset-state!)
    (binding [*ns* (create-ns 'cucumber.alt.backend)]
      (reset! cucumber-clj/glue a-glue)
      (load-script (.getPath resource))
      (doseq [{:keys [tag tag-expression fun location]} @hooks]
        (cucumber-clj/add-hook-definition tag tag-expression fun location))
      (doseq [step @steps]
        (add-step-definition step)))))

(defn- -buildWorld [cljb])

(defn- -disposeWorld [cljb])

(defn- -getSnippet [cljb step _]
  (.getSnippet cucumber-clj/snippet-generator step nil))

(defn- -setUnreportedStepExecutor [cljb executor]
  "executor")

(defmacro step-macros [& names]
  (cons 'do
        (for [name names]
          `(defmacro ~name [pattern# binding-form# & body#]
             (let [arg-meta# (mapv meta binding-form#)
                   context# (vec (filter (comp :context meta) binding-form#))
                   arg-meta# (vec (remove :context arg-meta#))
                   bindings# (vec (remove (comp :context meta) binding-form#))]
               `(swap! steps conj
                       {:pattern ~pattern#
                        :types ~arg-meta#
                        :fun 
                        (fn ~bindings#
                          (with-redefs-fn (deref redefs)
                            (fn []
                              (with-bindings (deref bindings)
                                (with-context ~context#
                                  (let [~'res# (do ~@body#)]
                                    (if (map? ~'res#)
                                      (swap! context merge ~'res#))))))))
                        :location '~{:file *file*
                                     :line (:line (meta ~'&form))}}))))))

(step-macros
 Given When Then And But)

(defn- hook-location [file form]
  {:file file
   :line (:line (meta form))})

(defmacro Before [tags & body]
  `(swap! hooks conj {:tag :before
                      :tag-expression ~tags
                      :fun (fn []
                            (let [res# (do ~@body)]
                              (if (map? res#)
                                (swap! context merge res#))))
                      :location ~(hook-location *file* &form)}))

(defmacro After [tags & body]
  `(swap! hooks conj {:tag :after
                      :tag-expression ~tags
                      :fun (fn []
                            (let [res# (do ~@body)]
                              (if (map? res#)
                                (swap! context merge res#))))
                      :location ~(hook-location *file* &form)}))

(defmacro add-bindings!
  [& {:as new-bindings}]
  (let [new-bindings
        (into {} (map (fn [[k v]] [(list 'var k) v]) new-bindings))]
    `(swap! bindings merge ~new-bindings)))

(defmacro add-redefs!
  [& {:as new-redefs}]
  (let [new-redefs
        (into {} (map (fn [[k v]] [(list 'var k) v]) new-redefs))]
    `(swap! redefs merge ~new-redefs)))
