(ns leiningen.cucumber
  (:require [clojure.java.io :as io]
            [cucumber.alt.utils :as utils]
            [leiningen.core.eval :refer [eval-in-project]]))

(defn cucumber
  "Passes command line options to Cucumber runtime. You can specify
   the runtime options using cucumber-options in project file"
  [project & args]
  (apply cucumber.alt.utils/runtime project args))
