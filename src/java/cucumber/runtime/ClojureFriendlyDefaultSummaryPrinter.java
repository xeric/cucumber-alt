package cucumber.runtime;

import cucumber.api.SummaryPrinter;

import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.io.Writer;
import java.util.List;

public class ClojureFriendlyDefaultSummaryPrinter implements SummaryPrinter {
    private final Writer writer;

    public ClojureFriendlyDefaultSummaryPrinter(Writer writer) {
	this.writer = writer;
    }

    public void print(cucumber.runtime.Runtime runtime) {
	ByteArrayOutputStream bout = new ByteArrayOutputStream();
	PrintStream out = new PrintStream(bout);
	out.println();
	printStats(runtime, out);
	out.println();
	printErrors(runtime, out);
	printSnippets(runtime, out);
	try {
	    writer.write(bout.toString());
	}catch(java.io.IOException ioe){
	    throw new RuntimeException(ioe);
	}
    }

    private void printStats(cucumber.runtime.Runtime runtime, PrintStream out) {
	runtime.printStats(out);
    }

    private void printErrors(cucumber.runtime.Runtime runtime, PrintStream out) {
	for (Throwable error : runtime.getErrors()) {
	    error.printStackTrace(out);
	    out.println();
	}
    }

    private void printSnippets(cucumber.runtime.Runtime runtime, PrintStream out) {
	List<String> snippets = runtime.getSnippets();
	if (!snippets.isEmpty()) {
	    out.append("\n");
	    out.println("You can implement missing steps with the snippets below:");
	    out.println();
	    for (String snippet : snippets) {
		out.println(snippet);
	    }
	}
    }
}
