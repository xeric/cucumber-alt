package cucumber.runtime;


public class TransformerFactory {

    public static interface ITransformer {
	Object transform(String transform);
    }

    public static cucumber.api.Transformer create(final ITransformer transformer){
	return new cucumber.api.Transformer<Object>(){

	    @Override
	    public boolean canConvert(Class clazz){
		return true;
	    }

	    @Override
	    public Object transform(String input){
		return transformer.transform(input);
	    }
	};
    }
	
}


    
    
