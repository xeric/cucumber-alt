(defproject xeric/cucumber-alt "0.1.0-SNAPSHOT"
  :javac-options ["-target" "1.6" "-source" "1.6" "-Xlint:-options"]
  :description "An alternative implementation of cucumber-jvm/clojure and lein-cucumber."
  :url "http://xeric.net/cucumber-alt"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [org.clojure/java.classpath "0.2.3"]
                 [info.cukes/cucumber-clojure "1.2.3"]]
  :source-paths ["src/clj"]
  :test-paths ["test/clj" "test-resources"]
  :java-source-paths ["src/java"]
  :aot [cucumber.alt.backend cucumber.alt.utils])
