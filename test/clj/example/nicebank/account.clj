(ns example.nicebank.account)

(defprotocol IDeposit
  (deposit [this amount]))

(defprotocol IWithdraw
  (withdraw [this amount]))

(defrecord CashSlot [balance]
  IDeposit
  (deposit [this amount]
    (update-in this [:balance] + amount))
  IWithdraw
  (withdraw [this amount]
    (update-in this [:balance] - amount)))

(defrecord Account [balance]
  IDeposit
  (deposit [this amount]
    (update-in this [:balance] + amount))
  IWithdraw
  (withdraw [this amount]
    (update-in this [:balance] - amount)))

(defrecord Teller [cash-slot])

(defn transact
  [account teller amount]
  {:teller (update-in teller [:cash-slot] deposit amount)
   :account (withdraw account amount)})
  
