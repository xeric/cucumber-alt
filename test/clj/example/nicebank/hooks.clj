(ns example.nicebank.hooks
  (:require [cucumber.alt.backend :refer :all]))

(def ^:dynamic *binding-demo* 10)

(def rebinded (constantly false))

(Before []
        (println "Called before")
        (add-bindings! *binding-demo* 20)
        (add-redefs! rebinded (constantly true))
        {:begin? true})

(After []
       (println "Called after!"))

