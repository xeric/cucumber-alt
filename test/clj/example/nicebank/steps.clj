(ns example.nicebank.steps
  (:require [cucumber.alt.utils :as utils]
            [example.nicebank.account :refer :all]
            [example.nicebank.hooks :as hooks]
            [cucumber.alt.backend :refer :all])
  (:import [example.nicebank.account CashSlot Account Teller]))


(defrecord Domain [account teller])

(defn new-domain
  [account]
  (let [cash-slot (CashSlot. 0)]
    (Domain. account (Teller. cash-slot))) )

(defn domain-transact
  [domain amount]
  (let [{:keys [teller account]}
        (transact (:account domain) (:teller domain) amount)]
    (Domain. account teller)))

(defn bigdecimal-transformer [s]
  (BigDecimal. s))

(Given #"^I have deposited \$(\d+) in my account$"
       [^:context begin?
        ^{:type BigDecimal :transformer bigdecimal-transformer} amount]
       (let [account (deposit (Account. 0) amount)]
         (assert (= hooks/*binding-demo* 20))
         (assert begin?)
         (assert (hooks/rebinded))
         (assert (= (:balance account) amount))
         {:domain (new-domain account)}))

(When #"^I request \$(\d+)$"
      [^:context domain
       ^{:type BigDecimal :transformer bigdecimal-transformer} amount]
      {:domain (domain-transact domain amount)})

(Then #"^\$(\d+) should be dispensed$"
      [^:context domain
       ^{:type BigDecimal :transformer bigdecimal-transformer} cash]
      (assert (= (get-in domain [:teller :cash-slot :balance]) cash)))

(And #"^the balance of my account should be \$(\d+.\d+)$"
     [^{:type BigDecimal :transformer bigdecimal-transformer} balance
      ^:context domain]
     (assert (= (get-in domain [:account :balance]) balance)))

(utils/test-feature "features/cash_withdrawal"
                    :plugin :pretty)
