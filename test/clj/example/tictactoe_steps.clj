(ns example.tictactoe-steps
  (:require [cucumber.alt.backend :refer :all]
            [cucumber.alt.utils :as utils]
            [cucumber.runtime.clj :as c]))

(defn to-board
  [board]
  (mapv vec (vec (.raw board))))

(Given #"^a board like this:$" [^cucumber.api.DataTable board]
       {:board (to-board board)})

(When #"^player x plays in row (\d+), column (\d+)$" [^Long row ^Long column]
      (with-context [board]
        {:board (assoc-in board [row column] "x")}))

(Then #"^the board should look like this:$"
      [^cucumber.api.DataTable expected]
      (with-context [board]
        (assert (= (to-board expected) board))))

(utils/test-feature "features/tictactoe" :plugin "pretty")

