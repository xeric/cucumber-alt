(ns example.checkout-steps
  (:require [cucumber.alt.backend :refer :all]
            [cucumber.alt.utils :as utils]
            [cucumber.runtime.clj :as c]
            [leiningen.checkout :as checkout]))

(Given #"^the price of a \"([^\"]*)\" is (\d+)c$"
  [name ^Long price]
  {:banana-price price})

(When #"^I checkout (\d+) \"([^\"]*)\"$"
  [^Long item-count item-name ^:context banana-price]
  {:total (checkout/total item-count banana-price)})

(Then #"^the total price should be (\d+)c$"
  [^Long result ^:context total]
  (assert (= total result)))

(utils/test-feature "features/checkout" :plugin "pretty")



            
