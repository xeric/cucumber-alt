(ns leiningen.checkout)

(defn total
  [count price]
  (* count price))
