(ns cucumber.alt.utils-test
  (:require [cucumber.alt.utils :as utils :refer :all]
            [clojure.java.io :as io]
            [clojure.test :refer :all]))

(deftest test-map->cmd-args
  (are [m args] (= (#'utils/map->cmd-args m) args)
    {:plugin :pretty} ["--plugin" "pretty"]
    {:plugin [:pretty :hello]} ["--plugin" "pretty" "--plugin" "hello"]
    {:dry-run :dry-run} ["--dry-run"]))

(deftest test-feature-path
  (testing "feature path is returned"
    (is (some? (#'utils/feature-path "features/checkout"))))
  (testing "exception is thrown if feature doesnot exist"
    (is (thrown? cucumber.runtime.CucumberException
                 (#'utils/feature-path "features/checkout_wrong")))))

(deftest test-env
  (testing "project settings if exists are used"
    (System/setProperty CUCUMBER-OPTIONS "env-settings")
    (let [e (#'utils/env {:cucumber-options "project-settings"})]
      (is (= (.get e CUCUMBER-OPTIONS) "project-settings"))))
  (testing "if project settings are not present, env settings are used"
    (System/setProperty CUCUMBER-OPTIONS "env-settings")
    (let [e (#'utils/env {})]
      (is (= (.get e CUCUMBER-OPTIONS) "env-settings"))))
  (testing "if no settings are present. empty value is returned"
    (System/clearProperty CUCUMBER-OPTIONS)
    (let [e (#'utils/env {})]
      (is (= (.get e CUCUMBER-OPTIONS) "")))))


(deftest test-runtime
  (io/delete-file "target/work.out" true)
  (let [res (runtime {:name "testing"} "--plugin" "pretty:target/work.out")]
    (is (not (:errors? res)))
    (is (.exists (io/file "target/work.out")))))

(deftest test-test-feature*
  (io/delete-file "target/work.out" true)
  (let [res (test-feature* "features/checkout"
                           "test/clj/example/checkout_steps.clj"
                           {:plugin "pretty:target/work.out"})]
    (is (not (:errors? res)))))
    
