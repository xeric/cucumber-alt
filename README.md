# Cucumber-alt

A clojure library as an alternative for the inbuild clojure support in [**cucumber**](http://cucumber.io). It also includes a lein plugin.

## Plugin 
The plugin is a thin wrapper around the cucumber command line interface. 

### Installation
Use this for user-level plugins:

Put `[xeric/cucumber-alt "0.1.0-SNAPSHOT"]` into the `:plugins` vector of your
`:user` profile, or if you are on Leiningen 1.x do `lein plugin install
xeric/cucumber-alt 0.1.0-SNAPSHOT`.

Use this for project-level plugins:

Put `[xeric/cucumber "0.1.0-SNAPSHOT"]` into the `:plugins` vector of your project.clj.

### Usage

    $ lein cucumber <options>

`options` are passed in the same way to the cucumber-jvm command line interface. e.g.

	 $ lein cucumber --plugin "pretty:output.txt"
	 
## Library
Add `features` to the `test resources` directory and `step definitions` as clojure files to the `test` directories.

### Context
A context a maintained across the steps to avoid using state vars. The return value of each step is merged with the context. To access the context add a parameter to a step with a `^:context` meta.

```clojure
(Before [] {:begin? true})

(Given #"^I have deposited \$(\d+) in my account$"
  [^:context begin? amount]
  (let [account (deposit (Account. 0) amount)]
    (assert begin?)
    (assert (= (:balance account) amount))
    {:domain (new-domain account)}))
            
```

### Type hints
Type hints are used for type conversion. 

```clojure
(Given #"^I have deposited \$(\d+) in my account$"
  [^Long amount]
  (let [account (deposit (Account. 0) amount)]
    (assert (= (:balance account) amount))
    {:domain (new-domain account)}))
``` 

### Transformers
Custom transformers can be added as pure functions

```clojure
(defn bigdecimal-transformer [s]
  (BigDecimal. s))

(Given #"^I have deposited \$(\d+) in my account$"
  [^{:type BigDecimal :transformer bigdecimal-transformer} amount]
  (let [account (deposit (Account. 0) amount)]
    (assert (= (:balance account) amount))
    {:domain (new-domain account)}))
```

### Usage with repl
At the end of a step definition file add a `test-feature` macro to connect it to the `feature` file. e.g.

```clojure
(ns example.checkout-steps
  (:require [cucumber.alt.backend :refer :all]
            [cucumber.alt.utils :as utils]
            [cucumber.runtime.clj :as c]
            [leiningen.checkout :as checkout]))

(Given #"^the price of a \"([^\"]*)\" is (\d+)c$" 
  [name ^Long price]
  {:banana-price price})

(When #"^I checkout (\d+) \"([^\"]*)\"$" 
  [^Long item-count item-name ^:context banana-price]
  {:total (checkout/total item-count banana-price)}))

(Then #"^the total price should be (\d+)c$" 
  [^:context result ^Long result]
  (assert (= total result))))

(utils/test-feature "features/checkout" :plugin "pretty")
```

### Example
Checkout examples in the test directories

## License

Copyright © 2015 Xeric Corporation

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
